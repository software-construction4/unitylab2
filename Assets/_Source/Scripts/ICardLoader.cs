﻿using System.Collections.Generic;
using Card;

namespace _Source.Scripts
{
  public interface ICardLoader
  {
    public List<CardAsset> LoadAllCard(string path);


    public void UnloadCard(CardAsset cardAsset);
  }
}