﻿using Card;

namespace _Source.Scripts
{
  public interface ICardStorage
  {
    public void AddCard(CardView cardView);
  }
}