﻿using System.Collections.Generic;
using Card;
using UnityEngine;

namespace _Source.Scripts
{
  public class CardSpawner : MonoBehaviour
  {
    [SerializeField] private GameObject _cardPrefab;
    private LayoutStorage _layoutStorage;
    private readonly Dictionary<CardInstance, CardView> _cards = new();
    
    public List<CardAsset> _startCardsAssets = new();
    private ICardLoader _loader;
    private ICardStorage _storage;
    private CardMovement _movement;
   
    public void InitStartCard()
    {
      _startCardsAssets = _loader.LoadAllCard("StartCards");
      foreach (CardAsset startCardsAsset in _startCardsAssets)
      {
          _storage.AddCard(CreateCardView(CreateCard(startCardsAsset)));
      }
    }

    public void Init(ICardStorage storage, ICardLoader loader, LayoutStorage layoutStorage,CardMovement movement)
    {
      _storage = storage;
      _loader = loader;
      _layoutStorage = layoutStorage;
      _movement = movement;
    }
    private CardView CreateCardView(CardInstance cardInstance )
    {
      GameObject card = Instantiate(_cardPrefab,_layoutStorage.getLayoutTransform(cardInstance.Layout));
      card.transform.SetParent(_layoutStorage.getLayoutTransform(cardInstance.Layout));
      CardView view = card.GetComponent<CardView>();
      view.Init(cardInstance,_movement);
      return view;
    }
    
    

    private CardInstance CreateCard(CardAsset cardAsset)
    {
      CardInstance cardInstance = new CardInstance(cardAsset);
      cardInstance.Layout = LayoutType.Deck;
      return cardInstance;
    }
  }
}