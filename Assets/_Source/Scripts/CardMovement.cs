﻿using System.Collections.Generic;
using Card;

namespace _Source.Scripts
{
  public class CardMovement
  {
    private LayoutStorage _layoutStorage;
    private CardStorage _cardStorage;

    public void  Init(LayoutStorage layoutStorage, CardStorage cardStorage)
    {
      _layoutStorage = layoutStorage;
      _cardStorage = cardStorage;
    }

    public void MoveToLayout(CardView cardView, LayoutType layoutType)
    {
      cardView.GetCardInstance().Layout = layoutType;
      cardView.transform.SetParent(_layoutStorage.getLayoutTransform(layoutType));
    }
    public void MoveStartCard(int numberForeachPlayer)
    {
      List<CardView> cards = _cardStorage.GetCardWithLayout(LayoutType.Deck);
      for (int i = 0; i < numberForeachPlayer; i++)
      {
        MoveToLayout(cards[i], LayoutType.FirstPlayer);
        cards[i].GetCardInstance().FaceUp = 1;
      }
      for (int i = numberForeachPlayer; i < 2*numberForeachPlayer; i++)
      {
        MoveToLayout(cards[i], LayoutType.SecondPlayer);
        cards[i].GetCardInstance().FaceUp = 1;
      }

    }
  }
}