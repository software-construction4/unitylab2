﻿using Card;
using UnityEngine;

namespace _Source.Scripts
{
  public class CardGame
  {
    private CardLoader _cardLoader;
    private CardSpawner _cardSpawner;
    private CardStorage _cardStorage;
    private LayoutStorage _layoutStorage;
    private CardMovement _cardMovement;
    public CardGame(CardStorage cardStorage, CardSpawner cardSpawner, CardLoader cardLoader, LayoutStorage layoutStorage, CardMovement cardMovement)
    {
      _cardStorage = cardStorage;
      _cardSpawner = cardSpawner;
      _cardLoader = cardLoader;
      _layoutStorage = layoutStorage;
      _cardMovement = cardMovement;
    }

    public void StartGame()
    {
        _cardSpawner.InitStartCard();
        _cardMovement.MoveStartCard(6);
    }
    
  }
}