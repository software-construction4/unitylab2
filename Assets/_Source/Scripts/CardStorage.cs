﻿using System;
using System.Collections;
using System.Collections.Generic;
using Card;

namespace _Source.Scripts
{
  public class CardStorage : ICardStorage
  {
    private  List<CardView> _cardViews = new();

    public List<CardView> GetCardWithLayout(LayoutType layoutType)
    {
      return _cardViews.FindAll(card => card.GetCardInstance().Layout == layoutType);
    }
    public List<CardView> GetCards()
    {
      return _cardViews;
    }
    
    public void Remove(CardView cardView)
    {
      if (cardView == null) throw new ArgumentNullException();
      _cardViews.Remove(cardView);
    }

    public void AddCard(CardView cardView)
    {
      if (cardView == null) throw new ArgumentNullException();
      _cardViews.Add(cardView);
    }
  }
}