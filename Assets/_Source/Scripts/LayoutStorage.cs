﻿using System;
using System.Collections.Generic;
using Card;
using UnityEngine;


public enum LayoutType
{
  FirstPlayer,
  SecondPlayer,
  Deck,
  Mid
}

namespace _Source.Scripts
{
  public class LayoutStorage : MonoBehaviour
  {
    [SerializeField] private GameObject _deck;
    [SerializeField] private GameObject _nearHand;
    [SerializeField] private GameObject _farHand;
    [SerializeField] private GameObject _mid;
    public Transform getLayoutTransform(LayoutType layoutType)
    {
      switch (layoutType)
      {
        case LayoutType.FirstPlayer:
          return _nearHand.transform;
        case LayoutType.SecondPlayer:
          return _farHand.transform;
        case LayoutType.Deck:
          return _deck.transform;
        case LayoutType.Mid: ;
          return _mid.transform;
        default:
          return _deck.transform;
      }
    }
    
    
  }
}