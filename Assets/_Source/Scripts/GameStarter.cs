﻿using UnityEngine;

namespace _Source.Scripts
{
  public class GameStarter : MonoBehaviour
  {
    private CardGame _cardGame;
    [SerializeField] CardSpawner _cardSpawner;
    [SerializeField] private LayoutStorage _layoutStorage;
    private CardMovement _cardMovement;
    private void Awake()
    {
      
      CardStorage cardStorage = new(); 
      
      CardLoader cardLoader = new();
      _cardMovement = new();
      _cardMovement.Init(_layoutStorage,cardStorage);
      _cardSpawner.Init(cardStorage, cardLoader,_layoutStorage,_cardMovement);
      
      _cardGame = new CardGame(cardStorage, _cardSpawner, cardLoader,_layoutStorage,_cardMovement);
      _cardGame.StartGame();
     
    }

    
  }
}