﻿using System.Collections.Generic;
using System.Linq;
using Card;
using UnityEngine;

namespace _Source.Scripts
{
  public class CardLoader : ICardLoader
  {
    public List<CardAsset> LoadAllCard(string path)
    {
      List<CardAsset> cardObject = Resources.LoadAll<CardAsset>(path).ToList(); 
      return cardObject;
    }

    public void UnloadCard(CardAsset cardAsset)
    {
      Resources.UnloadAsset(cardAsset);
    }
  }
}