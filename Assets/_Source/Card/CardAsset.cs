using UnityEngine;

namespace Card
{
  [CreateAssetMenu(fileName = "CardAsset", menuName = "SO/New CardAsset")]
  public class CardAsset : ScriptableObject
  {
    [field: SerializeField] public string Name { get; private set; }
    [field: SerializeField] public Sprite Sprite { get; private set; }
    [field: SerializeField] public Color Color { get; private set; }
  }
}