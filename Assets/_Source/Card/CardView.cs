﻿using System;
using System.Net.Mime;
using _Source.Scripts;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Card
{
  public class CardView : MonoBehaviour,IPointerClickHandler
  {
    private CardInstance _cardInstance;
    [SerializeField] private TextMeshProUGUI _cardName;
    [SerializeField] private Image _cardImage;
    [SerializeField] private Image _backgroundColor;
    [SerializeField] private Image _cardShirt;
    [SerializeField] private Button _moveButton;
    private CardMovement _cardMovement;
    public void RenameCard(string name)
    {
        _cardInstance.Name = name;
    }

    public CardInstance GetCardInstance()
    {
      return _cardInstance;
    }

    public void ReColorCard(Color color)
    {
        _cardInstance.Color = color;
    }

    public void ChangeSprite(Sprite sprite)
    {
       _cardInstance.Sprite = sprite;
    }
    public void Init(CardInstance cardInstance,CardMovement movement)
    {
      _cardInstance = cardInstance;
      _cardName.text = cardInstance.Name;
      _cardImage.sprite = cardInstance.Sprite;
      _backgroundColor.color= cardInstance.Color;
      _cardInstance.onNameChanged += Rename;
      _cardInstance.onCardShirtChanged += ReFaceUp;
      _moveButton.onClick.AddListener(HideButton);
      _moveButton.onClick.AddListener(MovetoMid);
      _cardMovement = movement;
    }

    private void MovetoMid()
    {
      _cardMovement.MoveToLayout(this,LayoutType.Mid);
    }

    private void HideButton()
    {
      _moveButton.gameObject.SetActive(false);
    }
    
    private void ReFaceUp(int indicator)
    {
      _cardShirt.gameObject.SetActive(false);
    }

    private void Rename(string newName)
    {
        _cardName.text = newName;
    }

    private void ReImageChanged(Sprite newSprite)
    {
      _cardImage.sprite = newSprite;
    }

    private void ReColor(Color newColor)
    {
      _backgroundColor.color = _cardInstance.Color;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
      _moveButton.gameObject.SetActive(true);
    }
    
  }
}