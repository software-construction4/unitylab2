﻿using UnityEngine;

namespace Card
{
  public class CardInstance
  {
    private CardAsset _cardAsset;
    
    public event System.Action<string> onNameChanged ;
    
    public event System.Action<Sprite> onSpriteChanged ;
    
    public event System.Action<Color> onColorChanged ;

    public event System.Action<int> onCardShirtChanged;
    
    private string _name;
    private Sprite _sprite;
    private Color _color;

    private int _faceUp;

    private LayoutType _layout;
    
    public CardInstance(CardAsset cardAsset)
    {
      Name = cardAsset.Name;
      Sprite = cardAsset.Sprite;
      Color = cardAsset.Color;
      FaceUp = 0;
    }


    public LayoutType Layout
    {
      get => _layout;
      set
      {
        _layout = value;
      }
    }
    public string Name
    {
      get => _name;
      set
      {
        _name = value;
        onNameChanged?.Invoke(value);
      }
    }

    public int FaceUp
    {
      get => _faceUp;
      set
      {
        _faceUp = value;
        onCardShirtChanged?.Invoke(value);
      }
    }
    
    public Sprite Sprite
    {
      get => _sprite;
      set
      {
        _sprite = value;
        onSpriteChanged?.Invoke(value);
      }
    }

    public Color Color
    {
      get => _color;
      set
      {
        _color = value;
        onColorChanged?.Invoke(value);
      }
    }
  }
}